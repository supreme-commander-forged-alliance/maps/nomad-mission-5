# Nomad campaign
The Nomad faction and campaign is a large project started and worked on by prominent members of the Forged Alliance Forever community.

## Nomad Mission 5
This repository contains the World Machine files that generates the map layout that the fifth mission of the Nomad campaign may use in the future. The map is based on sketches and concepts by the (game) designers, interpreted and made in World Machine by me.

## Version 0.1
![](/images/version-0.1.png)

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).